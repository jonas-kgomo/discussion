## Mathematics

List of areas in mathematics that are in need of a major [FOA](http://fairopenaccess.org/) journal.
* arXiv & area: area of maths based on [arXiv categories](https://arxiv.org/archive/math).
* FOA journals: column for (some) FOA journals that already exist in that area
* non-FOA to flip: non-FOA journal for which a FOA alternative is needed
* need: wish by X to have a FOA journal in that field
Please fill with wishes where you see a need, add FOA journals, and refine if necessary. 
See also the [discussion on this](https://gitlab.com/publishing-reform/discussion/issues/15) - and the [related discussion](https://gitlab.com/publishing-reform/discussion/issues/7).

|arXiv| area                      | FOA journals    | non-FOA to flip |need|
|----|----------------------------|-----------------|--------------------------------------|----|
| AG | Algebraic Geometry         | [AG](https://algebraicgeometry.nl/#) | | |
| AT | Algebraic Topology         | | | |
| AP | Analysis of PDEs           | | | |
| CT | Category Theory            | [Compositionality](http://www.compositionality-journal.org/) | | |
| CO | Combinatorics              | [AIC](https://advances-in-combinatorics.scholasticahq.com/), [EJC](http://www.combinatorics.org/), [SLdC](http://www.mat.univie.ac.at/~slc/)| JCTA, JCTB |@darijgrinberg|
| -> | Algebraic Combinatorics    | [AC](https://alco.centre-mersenne.org/) | | |
| CA | Classical Analysis and ODEs| | | |
| AC | Commutative Algebra        | | | |
| CV | Complex Variables          | | | |
| DG | Differential Geometry      | | | |
| DS | Dynamical Systems          | | | |
| FA | Functional Analysis        | | JFA, GAFA, Analysis & PDE |@benoit.kloeckner|
| GM | General Mathematics        | |Adv. Math. |  |
| GN | General Topology           | | | |
| GT | Geometric Topology         | | | |
| GR | Group Theory               | | | |
| HO | History and Overview       | | | |
| IT | Information Theory         | | | | 
| KT | K-Theory and Homology      | | | |
| LO | Logic                      | | | |
| MP | Mathematical Physics       | | | |
| MG | Metric Geometry            | | | |
| NT | Number Theory              | [Integers](http://math.colgate.edu/~integers/), [J. Théor. Nr. Bordx](http://jtnb.cedram.org/?lang=en) | | |
| NA | Numerical Analysis         | | | |
| OA | Operator Algebras          | | | |
| OC | Optimization and Control   | |MPA, DO, DAM | @mposs|
| PR | Probability                | [EJP](https://www.imstat.org/journals-and-publications/electronic-journal-of-probability/), [ECP](https://www.imstat.org/journals-and-publications/electronic-communications-in-probability/)| PTRF |  |
| QA | Quantum Algebra            | | | |
| RA | Rings and Algebras         | | | |
| -> | Non-commutative algebras   | | J. of Alg. |@darijgrinberg|
| SP | Spectral Theory            | | | |
| ST | Statistics Theory          | | | |
| SG | Symplectic Geometry        | | | |
| O  | Others                     | | | |
| -> | Undergraduate-level exposition| |American Mathematical Monthly, Mathematics Magazine, College Mathematics Journal|@darijgrinberg|
